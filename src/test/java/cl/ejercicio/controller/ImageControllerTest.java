package cl.ejercicio.controller;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;

import cl.ejercicio.configuration.Configuration;
import cl.ejercicio.exception.ConfigClientException;

class ImageControllerTest {
	
	private ImageController controller;

	@BeforeEach
	void setUp() throws Exception {
		controller = new ImageController();
	}
	
	@AfterEach
	void restObjects() {
		controller = null;
	}

	@Test
	void getImagesSuccessTest() throws ConfigClientException {
		Configuration configuration = new Configuration();
		configuration.setImages("https://dog.ceo/api/breed/{0}/images");
		ReflectionTestUtils.setField(controller, "configuration", configuration);
		Assert.assertNotNull(controller.getImages("dalmatian"));

	}
	
	@Test
	void getImagesFaillureTest() throws ConfigClientException {
		Configuration configuration = new Configuration();
		configuration.setImages("https://dog.ceo/api/breed/{0}/images");
		ReflectionTestUtils.setField(controller, "configuration", configuration);
		ConfigClientException configClientException = assertThrows(ConfigClientException.class, () ->{
			controller.getImages("cat");
			
		});
		Assert.assertTrue(configClientException.getMessage().contains("Ha ocurrido un error al procesar"));
	}

}
