package cl.ejercicio.controller;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;

import cl.ejercicio.configuration.Configuration;
import cl.ejercicio.exception.ConfigClientException;

class AllControllerTest {

	private AllController controller;

	@BeforeEach
	void setUp() throws Exception {
		controller = new AllController();
	}

	@Test
	void AllBreedsSuccessTest() throws ConfigClientException   {
		Configuration configuration = new Configuration();
		configuration.setAll("https://dog.ceo/api/breeds/list/all");
		ReflectionTestUtils.setField(controller, "configuration", configuration);
		Assert.assertNotNull(controller.allBreeds());
	}
	
	@Test
	void AllFailureTest() throws ConfigClientException {
		Configuration configuration = new Configuration();
		configuration.setImages("bad url");
		ReflectionTestUtils.setField(controller, "configuration", configuration);
		ConfigClientException configClientException = assertThrows(ConfigClientException.class, () ->{
			controller.allBreeds();
		});
		Assert.assertTrue(configClientException.getMessage().contains("Ha ocurrido un error al procesar"));
	}


}
