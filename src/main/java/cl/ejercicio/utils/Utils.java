package cl.ejercicio.utils;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import cl.ejercicio.beans.Breed;
import cl.ejercicio.model.KeyValueModel;


/**
 * Clase utilitaria con métodos utilizados en el desarrollo.
 * 
 * @author Tomás González Valdés
 * 02/01/2020
 * */

public class Utils {
	private static final String EMPTY_STRING = "";

	
	public static List<KeyValueModel> parseJson(String json) throws JSONException{
		JSONObject jsonResponse = new JSONObject(json.trim());
		ArrayList<KeyValueModel> responseList = new ArrayList<KeyValueModel>();
		
		jsonResponse.keySet().forEach(key -> {
			Object value = jsonResponse.get(key);
			KeyValueModel keyValueModel=new KeyValueModel(key.toString(), value.toString());
			responseList.add(keyValueModel);
		});

		

		return responseList;
	}
	
	public static List<Breed> proccessDogMessage(List<KeyValueModel> dogs) throws JSONException {
		List<Breed> breeds = new ArrayList<Breed>();
		
		dogs.stream().forEach(dog -> {
			List<KeyValueModel> keyValues = parseJson(dog.getValueName());
			keyValues.stream().forEach(keyValue -> {
				Breed breed = new Breed();
				breed.setBreedName(keyValue.getKeyName());
				if (!"[]".equals(keyValue.getValueName())) {
					String temp = keyValue.getValueName().replace("[", EMPTY_STRING).replace("]", EMPTY_STRING);
					List<String> subBreeds = getSubBreeds(temp); 
					if (subBreeds != null) {
						breed.setSubBreed(subBreeds);
					}
				}
				breeds.add(breed);
			});
				
		});

		
		return breeds;
		
	}
	

	private static List<String> getSubBreeds(String strSubBrees) {
		List<String> subBreeds = new ArrayList<String>();
		String [] strArr = strSubBrees.split(",");
		for (String strTmp : strArr) {
			String subBreed = strTmp;
			subBreeds.add(subBreed);
		}
		return subBreeds;
	}

}
