package cl.ejercicio.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@ConfigurationProperties("dog")
@Data
public class Configuration {
	private String all;
	private String images;

}
