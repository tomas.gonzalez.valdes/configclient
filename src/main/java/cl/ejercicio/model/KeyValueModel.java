package cl.ejercicio.model;

import lombok.Data;

@Data
public class KeyValueModel {
	private String keyName;
    private String valueName;
    
	public KeyValueModel(String keyName, String valueName) {
        this.keyName = keyName;
        this.valueName = valueName;
    }
}
