package cl.ejercicio.exception;

public class ConfigClientException extends Exception {

	private static final long serialVersionUID = -5479914440614521883L;
	
	public ConfigClientException(String message) {
		super(message);
	}
	
	public ConfigClientException(String message, Throwable cause) {
		super(message, cause);
	}
	
	
	

}
