package cl.ejercicio.beans;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;

@Data
public class Breed {
	private String breedName;
	private List<String> subBreed;
	
	public List <String> getSubBreed(){
		if (this.subBreed == null) {
			this.subBreed = new ArrayList<String>();
		}
		return this.subBreed;
	}

}
