package cl.ejercicio.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import cl.ejercicio.beans.Breed;
import cl.ejercicio.configuration.Configuration;
import cl.ejercicio.exception.ConfigClientException;
import cl.ejercicio.model.KeyValueModel;
import cl.ejercicio.utils.Utils;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(value="/breeds")
@Slf4j

/**
 * Controller que devuelve todas las razas de perros existentes en la api.
 * 
 * @author Tomás González Valdés
 * 02/01/2020
 * */

public class AllController {
	
	@Autowired
	private Configuration configuration;
	
	@GetMapping("/all")
	public  List<Breed> allBreeds() throws ConfigClientException {
		List<KeyValueModel> dogs = new ArrayList<KeyValueModel>();
		log.info("====> configuration: {}", configuration.getAll());
		List<Breed> breeds = new ArrayList<Breed>();
		try {
			URI uri = new URI(configuration.getAll());
			RestTemplate restTemplate = new RestTemplate();
			String result = restTemplate.getForObject(uri, String.class);
			dogs = Utils.parseJson(result);
			
			
			List<KeyValueModel> filteredDogs = dogs.stream().filter(item -> item.getKeyName().equals("message")).collect(Collectors.toList());
			breeds = Utils.proccessDogMessage(filteredDogs);
			
		} catch (Exception e) {
			log.error("Ha ocurrido un error al procesar el request {}", e.getCause());
			throw new ConfigClientException("Ha ocurrido un error al procesar url " + configuration.getAll(), e);
		}
		return  breeds;
	}

}
