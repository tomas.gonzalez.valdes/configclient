package cl.ejercicio.controller;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import cl.ejercicio.beans.Image;
import cl.ejercicio.configuration.Configuration;
import cl.ejercicio.exception.ConfigClientException;
import cl.ejercicio.model.KeyValueModel;
import cl.ejercicio.utils.Utils;
import lombok.extern.slf4j.Slf4j;



/**
 * Controller devuelve la url a las imágenes de una raza dada por parámetro.
 * @param breed: raza de perro a consultar.
 * 
 * @author Tomás González Valdés
 * 02/01/2020
 * */


@RestController
@RequestMapping(value="/images")
@Slf4j
public class ImageController {
	
	@Autowired
	private Configuration configuration;
	
	@GetMapping("/{breed}")
	public List<Image> getImages(@PathVariable String breed) throws ConfigClientException {
		List<Image> imagesResponse = new ArrayList<Image>();
		log.info("====> obteniendo imagenes de {} ", breed);
		String uri =  MessageFormat.format(configuration.getImages(), breed);
		log.info("====> uri: {}", uri);
		
		try {
			RestTemplate restTemplate = new RestTemplate();
			String result = restTemplate.getForObject(uri, String.class);
			List<KeyValueModel> images =  Utils.parseJson(result);
			List<KeyValueModel> filteredImages = images.stream().filter(item -> item.getKeyName().equals("message")).collect(Collectors.toList());
			
			filteredImages.forEach(image -> {
				String[] arrImage = image.getValueName().split(",");
				for (String aux : arrImage) {
					Image imageResponse = new Image();
					imageResponse.setImage(aux);
					imagesResponse.add(imageResponse);
				}
			});
			
		} catch (Exception e){
			log.error("Ha ocurrido un error", e);
			throw new ConfigClientException("Ha ocurrido un error al procesar la url: " + uri, e.getCause());
			
		}
		
		return imagesResponse;
	}

}
